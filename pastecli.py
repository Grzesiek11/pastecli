from pbwrap import Pastebin
import json, sys, getopt, os

with open('settings.json') as file:
    settings = json.load(file)

pb = Pastebin(settings["credentials"]["apiKey"])
if bool(settings["credentials"]["login?"]):
    pb.authenticate(settings["credentials"]["username"], settings["credentials"]["password"])

args = sys.argv

if args[1] == 'post':
    opts, args = getopt.getopt(sys.argv[2:], "n:c:f:e", ["name=", "content=", "file=", "editor"])
    filePath = None
    for opt, arg in opts:
        if opt in ("-n", "--name"):
            name = arg
        elif opt in ("-c", "--content"):
            content = arg
        elif opt in ("-f", "--file"):
            filePath = arg
        elif opt in ("-e", "--editor"):
            os.system('%s %s' % (settings["editor"], settings["tempFile"]))
            file = open(settings["tempFile"], 'r') 
            content = file.read()
            file.close()
            os.remove(settings["tempFile"])
    if filePath is not None:
        file = open(filePath, 'r') 
        content = file.read()
        file.close()
    url = pb.create_paste(api_paste_code=content, api_paste_private=0, api_paste_name=name, api_paste_expire_date=None, api_paste_format=None)
    print(url)