# PasteCLI

PasteCLI is an unofficial CLI tool for pastebin.com. You can easily make a paste and instantly get the link from entering name and text, pasting it in your favourite editor or just make it from existing file.

# Commands

`paste -n <name> [-c <content>] [-f <pathToFile>] [-e]`

# settings.json

**General structure:**

```
{
    "credentials" : {
        "login?" : 0,
        "username" : "",
        "password" : "",
        "apiKey" : ""
    },
    "editor" : "nano",
    "tempFile" : "/tmp/pastecli.tmp"
}
```

In `credentials` section there are `login?` that is bool value and specifies if you want to be logged in or not (you can paste things even if you are anonymus). `username` and `password` are filled if you want to login. `apiKey` is your API key, you can get it from https://pastebin.com/api.php or get from friend. It's needed to acces the API. The `editor` variable is the command for invoking your favourite editor. Use `notepad` on Windows if you don't have anything else. `tempFile` is needed for one function. You can change it for `%appdata%\pastecli.tmp` if you are using Windows.